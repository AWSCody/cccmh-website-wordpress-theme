<?php
define('WP_USE_THEMES', false);
include_once dirname(__FILE__) . "/Core/Display.Class.php";

Class Common {

	private $data = array();
	private $filters = array();
	private $filter_opening = "{{";
	private $filter_close = "}}";

	public function setDataArray($arr) {
		$this -> data = $arr;
	}

	public function addData($key, $value) {
		$this -> data[$key] = $value;
	}

	public function addFilter($key, $value) {
		$this -> filters[$key] = $value;
	}

	public function compile() {
		$t_data = array();
		foreach ($this->data as $key => $value) {
			if (!(strpos($key, "_NO_FILTER") > 0)) {
				$t_data[$key] = $value;
				foreach ($this->filters as $r_key => $r_value) {
					$t_data[$key] = str_replace($this -> filter_opening . $r_key . $this -> filter_close, $r_value, $t_data[$key]);

				}
			}

		}
		$this -> data = $t_data;
		return $this -> data;
	}

}

$common = new Common();

$data = (!is_array($data) ? array() : $data);

global $wp_query;
$postid = $wp_query -> post -> ID;
$data["POST_ID"] = $post -> ID;
$original_post_id = $postid;
$data["SITE_TITLE"] = get_bloginfo("name", $filter);
$data["THEME_URL"] = get_bloginfo("template_url", $filter);
$data["SITE_URL"] = get_bloginfo("wpurl", $filter);
$data["BASE_URL"] = get_bloginfo("wpurl", $filter);
$data["PAGE_TITLE"] = get_the_title($post -> ID);
$data["PAGE_HEADLINE"] = (is_null($data["PAGE_HEADLINE"])) ? get_post_meta($post -> ID, 'PAGE_HEADLINE', true) : $data["PAGE_HEADLINE"];
$data["CONTENT"] = (is_null($data["CONTENT"])) ? $post -> post_content : $data["CONTENT"];
$data["TIMESTAMP"] = time();

$data["POST_CATEGORY"] = get_post_meta($post -> ID, 'POST_CATEGORY', true);


$common -> addFilter("SITE_URL", $data["SITE_URL"]);
$common -> addFilter("THEME_URL", $data["THEME_URL"]);
$data['HEADER'] = "Common/Header.html";
$data['NAVIGATION'] = "Common/Navigation.html";
//$data['login'] = "Common/Login.html";

$data['FOOTER'] = "Common/Footer.html";

$menu_name = 'header-menu';

if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
	$menu = wp_get_nav_menu_object($locations[$menu_name]);
	$menu_items = wp_get_nav_menu_items($menu -> term_id);
}

// $menu_list now ready to output
$data["NAVIGATION_MENU_ITEMS"] = $menu_items;

//var_dump($menu_items);
//var_dump($post);
Display_Component::exposeFunction('truncate', 'truncate');
Display_Component::exposeFunction("bloginfo", "bloginfo");
Display_Component::exposeFunction("dynamic_sidebar", "dynamic_sidebar");
Display_Component::exposeFunction("is_active_sidebar", "is_active_sidebar");
Display_Component::exposeFunction("get_post_meta", "get_post_meta");

Display_Component::exposeFunction("get_the_author", "get_the_author");

Display_Component::exposeFunction("the_date", "the_date");

Display_Component::exposeFunction("the_time", "the_time");
Display_Component::exposeFunction("var_dump", "var_dump");
Display_Component::exposeFunction("the_modified_date", "the_modified_date");
Display_Component::exposeFunction("date", "date");
Display_Component::exposeFunction("strtotime", "strtotime");
Display_Component::exposeFunction("mysql2date", "mysql2date");
Display_Component::exposeFunction("get_settings", "get_settings");
Display_Component::exposeFunction("get_permalink", "get_permalink");
Display_Component::exposeFunction("time", "time");


//include the core functionality for post filtering.
include dirname(__FILE__) . "/Core/WP-Functions/wp-functions.php";



//Include the Blog Block.
include dirname(__FILE__)."/Blocks/blog.php";
?>