<?php
/*
 Template Name: Blog Postings
 */
remove_filter('the_content', 'wpautop');
include dirname(__FILE__) . "/common.php";

$data["PAGE"] = "Pages/Blog.html";

$data["POSTSW"] = array();
$data["POST_CATEGORY"] = "News";

if (!is_null($data["POST_CATEGORY"])) {
	query_posts("category_name=" . $data["POST_CATEGORY"] . "&order=desc");
	if (have_posts()) :
		while (have_posts()) : the_post();

			global $wp_query;
			$post = $wp_query -> post;
			$img = "";
			//read the post meta for the value of FIRST, and push it to the front.
			$post_order = get_post_meta($post -> ID, 'FIRST', true);
			$img = get_post_meta($post -> ID, 'IMG', true);

			//if there is no image then just use a placeholder one.
			if ($img != "") {
				$post -> IMG = $img;

			} else {
				$post -> IMG = "http://placehold.it/600x400";
			}

			if ($post_order == "true") {
				array_unshift($data["POSTSW"], $post);
			} else {
				array_push($data["POSTSW"], $post);
			}

		endwhile;
	endif;

	//print_r($data["POSTS"]);
}

$common -> setDataArray($data);
$data = $common -> compile();

Display_Component::renderDisplay(dirname(__FILE__) . "/Templates", "Site.html", $data);
?>

