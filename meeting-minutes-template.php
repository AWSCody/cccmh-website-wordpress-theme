<?php
/*
 Template Name: Meeting Minute Postings
 */
remove_filter( 'the_content', 'wpautop' );
include dirname(__FILE__) . "/common.php";

$data["PAGE"]="Pages/Minutes.html";

$data["POSTS"] = array();
$data["POST_CATEGORY"]="Meeting-Minutes";

if (!is_null($data["POST_CATEGORY"])) {
	query_posts("category_name=" . $data["POST_CATEGORY"] . "&order=desc");
	if (have_posts()) :
		while (have_posts()) : the_post();
	
			global $wp_query;
			$post = $wp_query -> post;
			//read the post meta for the value of FIRST, and push it to the front.
			$post_order = get_post_meta($post -> ID, 'FIRST', true);
			
			
			if ($post_order == "true") {
				array_unshift($data["POSTS"],$post);
			}else{
				array_push($data["POSTS"], $post);
			}
			
		endwhile;
	endif;

	//print_r($data["POSTS"]);
}



Display_Component::exposeFunction("get_the_author", "get_the_author");

Display_Component::exposeFunction("the_date", "the_date");

Display_Component::exposeFunction("the_time", "the_time");
Display_Component::exposeFunction("var_dump", "var_dump");
Display_Component::exposeFunction("the_modified_date", "the_modified_date");
Display_Component::exposeFunction("date", "date");
Display_Component::exposeFunction("strtotime", "strtotime");
Display_Component::exposeFunction("mysql2date", "mysql2date");
Display_Component::exposeFunction("get_settings", "get_settings");
Display_Component::exposeFunction("get_permalink", "get_permalink");


$common -> setDataArray($data);
$data = $common -> compile();



Display_Component::renderDisplay(dirname(__FILE__) . "/Templates", "Site.html", $data);
?>

