<?php
/*
 Template Name: Post Template
 */

include dirname(__FILE__) . "/common.php";

$data["CONTENT_TITLE"] = get_post_meta($original_post_id, 'CONTENT_TITLE', true);
$data["CONTENT_CAPTION"] = get_post_meta($original_post_id, 'CONTENT_CAPTION', true);

$data["SIDE_1_IMAGE"] = get_post_meta($original_post_id -> ID, 'SIDE_1_IMAGE', true);
$data["SIDE_2_IMAGE"] = get_post_meta($original_post_id -> ID, 'SIDE_2_IMAGE', true);


$data["PAGE"]="Pages/Post.html";
$common -> setDataArray($data);
$data = $common -> compile();

/*Since this is an object we are going to put it in the array after we compile the other variables.*/
$data["POST"]=$post;


$post_categories = wp_get_post_categories( $post->ID );
$cat = get_category($post_categories[0]);
//$data["CATEGORY"]=$cat;



Display_Component::exposeFunction("get_the_author", "get_the_author");

Display_Component::exposeFunction("the_date", "the_date");

Display_Component::exposeFunction("the_time", "the_time");
Display_Component::exposeFunction("var_dump", "var_dump");
Display_Component::exposeFunction("the_modified_date", "the_modified_date");
Display_Component::exposeFunction("date", "date");
Display_Component::exposeFunction("strtotime", "strtotime");
Display_Component::exposeFunction("mysql2date", "mysql2date");
Display_Component::exposeFunction("get_settings", "get_settings");


$data["CATEGORY_NAME"]=$cat->name;

Display_Component::renderDisplay(dirname(__FILE__) . "/Templates", "Site.html", $data);
?>