<?php

add_theme_support('menus');

if (function_exists('register_sidebar')) {
	register_sidebar(array('name' => 'Right Sidebar 1', 'id' => 'right-sidebar-1', 'description' => 'Appears as the sidebar on the right top.', 'before_widget' => '<div id="%1$s" class="entry sidebar-widget widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h4 class="title">', 'after_title' => '</h4>', ));

	register_sidebar(array('name' => 'Right Sidebar 2', 'id' => 'right-sidebar-2', 'description' => 'Appears as the sidebar on the right top.', 'before_widget' => '<div id="%1$s" class="entry sidebar-widget widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h4 class="title">', 'after_title' => '</h4>', ));

	register_sidebar(array('name' => 'Right Sidebar 3', 'id' => 'right-sidebar-3', 'description' => 'Appears as the sidebar on the right top.', 'before_widget' => '<div id="%1$s" class="entry sidebar-widget widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h4 class="title" style="display:none;">', 'after_title' => '</h4>', ));

	/*Left Sidebar*/

	register_sidebar(array('name' => 'Left Sidebar 1', 'id' => 'left-sidebar-1', 'description' => 'Appears as the sidebar on the right top.', 'before_widget' => '<div id="%1$s" class="entry sidebar-widget widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h4 class="title">', 'after_title' => '</h4>', ));

	register_sidebar(array('name' => 'Left Sidebar 2', 'id' => 'left-sidebar-2', 'description' => 'Appears as the sidebar on the right top.', 'before_widget' => '<div id="%1$s" class="entry sidebar-widget widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h4 class="title">', 'after_title' => '</h4>', ));

	register_sidebar(array('name' => 'Left Sidebar 3', 'id' => 'left-sidebar-3', 'description' => 'Appears as the sidebar on the right top.', 'before_widget' => '<div id="%1$s" class="entry sidebar-widget widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h4 class="title">', 'after_title' => '</h4>', ));

}

add_action('admin_menu', 'my_plugin_menu');

function my_plugin_menu() {
	add_options_page('My Plugin Options', 'My Plugin', 'manage_options', 'my-unique-identifier', 'my_plugin_options');
}

function my_plugin_options() {
	if (!current_user_can('manage_options')) {
		wp_die(__('You do not have sufficient permissions to access this page.'));
	}
	echo '
	<div class="wrap">
	';
	echo '
	<p>
	Here is where the form would go if I actually had options.
	</p>
	';
	echo '
	</div>';
}

function register_my_menu() {
	register_nav_menu('header-menu', __('Header Menu'));
}

add_action('init', 'register_my_menu');

/*Register the navigation menu*/
function register_my_menus() {
	register_nav_menus(array('header-menu' => __('Header Menu'), 'extra-menu' => __('Extra Menu')));
}

add_action('init', 'register_my_menus');

remove_filter('the_content', 'wpautop');

/**
 * Function to split strings, and limit lenths for formatting.
 */
function truncate($string, $line_size, $text_length) {
	$splits = 0;
	if (strlen($string) < $text_length) {
		return $string;
	} else {
		//break the string into breaks
		for ($i = $line_size; $i < strlen($string); $i++) {
			//if i= line size then break;
			if ($i % $line_size) {

				array_shift(str_split($string, $i)) . "<br>";

			}

		}

		return array_shift(str_split($string, $text_length)) . "...";

	}

}
?>