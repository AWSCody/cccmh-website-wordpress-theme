<?php
/**
 * Blog Block
 * by Cody Adkins
 * 12/17/2013
 * 
 * Basic Includable Block.
 */
 
 
 //Blog Column
$data["BLOG_POSTS"] = array();
$data["POST_CATEGORY"] = "News";

if (!is_null($data["POST_CATEGORY"])) {
	query_posts("category_name=" . $data["POST_CATEGORY"] . "&order=desc");
	if (have_posts()) :
		while (have_posts()) : the_post();

			global $wp_query;
			$post = $wp_query -> post;
			//read the post meta for the value of FIRST, and push it to the front.
			$post_order = get_post_meta($post -> ID, 'FIRST', true);
			$img = get_post_meta($post -> ID, 'IMG', true);
			//$post -> img = $img;
			
			$post->IMG=$img;
			

			$post -> year = get_the_time('Y');
			$post -> month = get_the_time('m');
			$post -> day = get_the_time('d');
			
			$post->author = get_the_author();

			if ($post_order == "true") {
				array_unshift($data["BLOG_POSTS"], $post);
			} else {
				array_push($data["BLOG_POSTS"], $post);
			}

		endwhile;
	endif;

	//print_r($data["POSTS"]);
}


Display_Component::exposeFunction("get_settings", "get_settings");
Display_Component::exposeFunction("mysql2date", "mysql2date");


?>