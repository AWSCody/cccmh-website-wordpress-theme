<?php
/*
 Template Name: Hero Page
 */

include dirname(__FILE__) . "/common.php";

$data["CONTENT_TITLE"] = get_post_meta($original_post_id, 'CONTENT_TITLE', true);
$data["CONTENT_CAPTION"] = get_post_meta($original_post_id, 'CONTENT_CAPTION', true);

$cols = array();
foreach (get_post_meta( $original_post_id, 'COLUMN', false ) as $col) {
	$nse = explode(';', $col);
	$ns = array();
	$ns["ID"] = $nse[0];
	$ns["TITLE"] = $nse[1];
	$ns["HEADING"] = $nse[2];
	$ns["TEXT"] = $nse[3];
	$cols[] = $ns;
}

$data["SLIDES"] = $slides;
$data["SOCIALS"] = get_post_meta($original_post_id, 'SOCIALS', true);

//page specifics.
$data["COLS"] = "Pages/Components/Cols.html";
$data["FEATURED"] = "Pages/Components/Featured.html";
$data["PAGE"] = "Pages/Hero.html";
$data["SLIDER"] = true;

$common -> setDataArray($data);
$data = $common -> compile();

$slides = array();
foreach (get_post_meta( $original_post_id, 'SLIDE', false ) as $slide) {
	$nse = explode(';', $slide);
	$ns = array();
	$ns["ID"] = $nse[0];
	$ns["URL"] = $nse[1];
	$ns["HEADING"] = $nse[2];
	$ns["TITLE"] = $nse[3];
	$ns["TEXT"] = $nse[4];
	$slides[] = $ns;
}

$data["SLIDES"] = $slides;
$data["COLUMNS"] = $cols;


$data["BANNER_MESSAGE"] = get_post_meta($original_post_id, 'BANNER_MESSAGE', true);
$data["BANNER_HTML"] = get_post_meta($original_post_id, 'BANNER_HTML', true);


Display_Component::renderDisplay(dirname(__FILE__) . "/Templates", "Site.html", $data);
?>