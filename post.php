<?php
/*
 Template Name: Post Template
 */

include dirname(__FILE__) . "/common.php";

$data["CONTENT_TITLE"] = get_post_meta($original_post_id, 'CONTENT_TITLE', true);
$data["CONTENT_CAPTION"] = get_post_meta($original_post_id, 'CONTENT_CAPTION', true);

$data["SIDE_1_IMAGE"] = get_post_meta($original_post_id -> ID, 'SIDE_1_IMAGE', true);
$data["SIDE_2_IMAGE"] = get_post_meta($original_post_id -> ID, 'SIDE_2_IMAGE', true);


$data["PAGE"]="Pages/Page.html";
$common -> setDataArray($data);
$data = $common -> compile();

Display_Component::renderDisplay(dirname(__FILE__) . "/Templates", "Site.html", $data);
?>